# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

###  `yarn start` or `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### API
- https://rapidapi.com/hub
- ExerciseDB

### Final Project

<img src="./src/assets/capa/capa-1.png" />

---

<img src="./src/assets/capa/capa-2.png" />

---

<img src="./src/assets/capa/capa-3.png" />

--- 

<img src="./src/assets/capa/capa-4.png" />